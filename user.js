// -------------------------------------------------
// Proyecto  Techu - Ejercicio de mlab User
// Version 1.0.0
// Lidice Stankervicaite -  2018
// -------------------------------------------------

// Descargo la dependencia vinculada a express
//var express = require('express');
var bodyParser = require('body-parser');
var requestJSON=require('request-json');
var newID=0;
var app = express();

// entorno ----------------------------------
let entorno = require('./entorno.js');
//let app = require('./user.js');

var URLbase= entorno.URLbase ;
var baseMLabURL = entorno.baseMLabURL;
var apikeyMLab = entorno.apikeyMLab;

var port=process.env.PORT||3000;
app.use(bodyParser.json());
// -------------------------------------------------

//++++++++ users visto en clase++++++++---------------------------------------------------

// GET users consumiendo API REST de mLab
app.get(URLbase + 'users',
  function(req, res) {
    console.log("GET /api-uruguay/v1");
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});

// Petición GET con id en mLab
app.get(URLbase + 'users/:id',
  function (req, res) {
    console.log("GET /api-uruguay/v1:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.sendStatus(500);
        } else {
          if(body.length > 0) {
            response = body[0];
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.sendStatus(404);
          }
        }
        res.send(response);
      });
});

// Petición POST con id en mLab
app.post(URLbase + 'users',
  function(req, res){
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?' + apikeyMLab,
      function(error, respuestaMLab, body){
        newID = body.length + 1;
        console.log(req.body);
        console.log("newID:" + newID);
        var newUser = {
          "id" : newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
           };

            clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, newUser,
            function(error, respuestaMLab, body){
              res.send(body); //aqui se puede controlar error con la variable error

          });
      });
  });


// Petición PUT con id en mLab
app.put(URLbase + 'users/:id',
  function (req, res) {
    console.log("put /api-uruguay/v1:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString  + apikeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
        var response = {};
        if(err) {
           res.status(500).send('Error obteniendo usuario');
        } else {
          if(body.length > 0) {
              // Obtiene el primer elemento del array
                  response = body[0];
                  var userIdInterno =  body[0]._id.$oid; // id de mongo
                  var clienteMlab = requestJSON.createClient(baseMLabURL);
                   console.log("Respuesta mLab id interno."+  body[0]._id.$oid);
                   clienteMlab.put("user/"+ userIdInterno + '?'+ apikeyMLab, req.body,
                     function(err, respuestaMLab, usrResponse){
                    res.send(usrResponse);
                   });
          }
          else {
               res.status(404).send('Usuario no encontrado');
          };
        };
      });
  });


    // Petición delete con id en mLab
    app.delete(URLbase + 'users/:id',
      function (req, res) {
        console.log("put /api-uruguay/v1:id");
        console.log(req.params.id);
        var id = req.params.id;
        var queryString = 'q={"id":' + id + '}&';
        var httpClient = requestJSON.createClient(baseMLabURL);
        httpClient.get('user?' + queryString  + apikeyMLab,
          function(err, respuestaMLab, body){
            console.log("Respuesta mLab correcta.");
            var response = {};
            if(err) {
                 res.status(500).send('Error obteniendo usuario');
            } else {
              if(body.length > 0) {
                  // Obtiene el primer elemento del array
                  response = body[0];
                  var userIdInterno =  body[0]._id.$oid; // id de mongo
                  var clienteMlab = requestJSON.createClient(baseMLabURL);
                   console.log("Respuesta mLab id interno."+  body[0]._id.$oid);
                   clienteMlab.delete("user/"+ userIdInterno + '?'+ apikeyMLab,
                     function(err, respuestaMLab, usrResponse){
                      res.status(200).send('Usuario Borrado');
                   });
              } else {
                   res.status(404).send('Usuario no encontrado');
              }
            }
          });
      });

// ---------Exporto las funciones ----------------------------------------




// -------------------------------------------------

// Listen recibe como parametro el puerto desde el cual escucha
app.listen(port);
console.log('Escuchando en el puerto 3000...');
