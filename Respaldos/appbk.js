// console.log("Hola Uruguay");

// Descargo la dependencia vinculada a express
var express = require('express');
var bodyParser = require('body-parser');

// Variables y constantes
var app = express();
var usersFile = require("./users.json");
var cuentasFile = require("./cuentas.json");
const URI= '/api-uruguay/v1/';
var requestJSON=require('request-json');

var baseMLabURL = "https://api.mlab.com/api/1/databases/techubduruguay/collections/"
var apikeyMLab = "lK0qtGN5GVG_goeTGdWeGOCWwU8TLvuV"

// variable del puerto
//var port=3000;
// variable del puerto desde una variable de ambiente y si no encuntra da 3000
var port=process.env.PORT||3000;
app.use(bodyParser.json());


// Traigo una peticion asinc rona y generica - funcion call-back, request y response
//app.get('/',
//    function(req, res) {
//      res.send('Hola Api desde el Node ....xxx.')
//    });


// Get users
   app.get(URI+'users',
      function get(req, res) {
      res.send(usersFile);
      console.log('GET users');
    });

    // Get de un usuario especifico
       app.get(URI+'users/:id',
          function (req, res) {
             let idUser = req.params.id;
             res.send(usersFile[idUser -1]);
             console.log('GET users especifico');
        });

  // Post users
           app.post(URI+'users',
              function (req, res)   {
                console.log('Post users');
                console.log(req.body);
                let tam = usersFile.length + 1;
            //    var userNuevo=req.body;
                var userNuevo=
              {'id':tam,
               'first_name': req.body.first_name,
               'last_name': req.body.last_name,
               'email': req.body.email,
                'password': req.body.password
             };
                usersFile.push(userNuevo);

                res.send({'mensaje:' : 'Nuevo usuario añadido', userNuevo});
            });

            // Post users header
                     app.post(URI+'users_post',
                        function (req, res)   {
                          console.log('Post users en el headers');
                          console.log(req.headers);
                          let tam = usersFile.length + 1;
                      //    var userNuevo=req.body;
                          var userNuevo=
                        {'id':tam,
                         'first_name': req.headers.first_name,
                         'last_name': req.headers.last_name,
                         'email': req.headers.email,
                          'password': req.headers.password
                       };
                          usersFile.push(userNuevo);
                          res.send({'mensaje:' : 'Nuevo usuario añadido headers', userNuevo});
                      });

          // Put users
            app.put(URI+'users/:id',
                function (req, res)   {
                  console.log('Put users');
                  var existe = true;
                  let idUpdate=req.params.id;
                  if (usersFile[idUpdate-1] == undefined)
                     {console.log('Usuario no existe');
                     existe = false;}
                     else {
                       var userAct=
                     {'id':idUpdate,
                      'first_name': req.body.first_name,
                      'last_name': req.body.last_name,
                      'email': req.body.email,
                       'password': req.body.password };
                       usersFile[idUpdate-1]=userAct;
                     }
                     var msgResponse = existe ? {'msg':'Update OK'} : {'msg':'Update Fallo'};
                     var statusCode = existe ? 200 : 400;
                     res.status(statusCode).send(msgResponse);
              //     res.send(msgResponse) .status(statusCode) ;
                  });

        // Delete users
                app.delete(URI+'users/:id',
                        function (req, res)   {
                          console.log('Delete users');
                          var existe = true;
                          let idDelete=req.params.id;
                          if (usersFile[idDelete-1] == undefined)
                             {console.log('Usuario no existe');
                             existe = false;}
                             else {
                               usersFile.splice([idDelete-1],1);
                             }
                             var msgResponse = existe ? {'msg':'Update OK'} : {'msg':'Update Fallo'};
                             var statusCode = existe ? 200 : 400;
                             res.status(statusCode).send(msgResponse);
                      //     res.send(msgResponse) .status(statusCode) ;
                          });

      // Get con query string
      //  app.get(URI+"'users?qname='Lidice'&qlast_name='Stanker'",
          app.get(URI+'user',
                function (req, res)   {
                    console.log('Get con query');
                    console.log(req.query);
                    let encontrado = false;
                    let msgResponse = {'msg': 'Usuario NO encontrado: '};
                  //  for (user in usersFile)  {
                    for (i=0; i < usersFile.length && encontrado; i++)  {
                //       console.log(req.query);
                        console.log(usersFile[i]);
                //          console.log(user.first_name + ' ' + user.last_name);

                      if (user.usersFile[i].first_name ==  req.query.qname && usersFile[i].last_name ==  req.query.qlast_name)
                         {
                         let msgResponse = {'msg': 'Usuario encontrado: ', user};
                        encontrado = true;

                         };
                      };

                      var statusCode = encontrado ? 200 : 400;
                      res.status(statusCode).send(msgResponse)
                  //   res.send(msgResponse);
                  //  usersFile.filter() -- es otra forma
                  });

    // Get cuentas
    //app.get(URI+'cuentas',
    //   function get(req, res) {
    //      res.sendfile('cuentas.json',{root:__dirname});
    //    });

    // Get cuentas
        app.get(URI+'cuentas',
           function get(req, res) {
              res.send(cuentasFile);
            });

    // Get de una cuenta especifica de forma asincrona con funcion anonima
        app.get(URI+'cuentas/:id',
            function (req, res) {
              let idCuenta = req.params.id;
              res.send(cuentasFile[idCuenta -1]);
              console.log('GET cuenta especifica');
            });

    // Post Cuentas body
        app.post(URI+'cuentas',
        function (req, res)   {
              console.log('Post Cuentas');
              console.log(req.body);
              let tam = cuentasFile.length + 1;
          //    var userNuevo=req.body;
              var cuentasNuevo=
                        {'id':tam,
                         'idUser': req.body.idUser,
                        'IBAN': req.body.IBAN,
                         'moneda': req.body.moneda,
                        'saldo': req.body.saldo
                       };
                          usersFile.push(cuentasNuevo);

                          res.send({'mensaje:' : 'Nueva cuenta añadida', cuentasNuevo});
                      });



// Listen recibe como parametro el puerto desde el cual escucha
app.listen(port);
console.log('Escuchando en el puerto 3000...');
