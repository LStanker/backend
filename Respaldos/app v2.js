// -------------------------------------------------
// Proyecto  Techu - Ejercicio de Mongo
// Version 1.0.0
// Lidice Stankervicaite -  2018
// -------------------------------------------------

// Descargo la dependencia vinculada a express
var express = require('express');
var bodyParser = require('body-parser');
var requestJSON=require('request-json');
var newID=0;


// Variables y constantes
var app = express();
const URLbase= '/api-uruguay/v1/';
var baseMLabURL = "https://api.mlab.com/api/1/databases/techubduruguay/collections/";

var apikeyMLab = "apiKey=lK0qtGN5GVG_goeTGdWeGOCWwU8TLvuV";

var port=process.env.PORT||3000;
app.use(bodyParser.json());

//======================================

// GET users consumiendo API REST de mLab
app.get(URLbase + 'users',
  function(req, res) {
    console.log("GET /api-uruguay/v1");
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});

// Petición GET con id en mLab
app.get(URLbase + 'users/:id',
  function (req, res) {
    console.log("GET /api-uruguay/v1:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});


// Petición POST con id en mLab
app.post(URLbase + 'users',
  function(req, res){
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?' + apikeyMLab,
      function(error, respuestaMLab, body){
        newID = body.length + 1;
        console.log("newID:" + newID);
        var newUser = {
          "id" : newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
           };

            clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, newUser,
            function(error, respuestaMLab, body){
              res.send(body); //aqui se puede controlar error con la variable error

          });
      });
  });


  //  Petición PUT con id en mLab
  app.put(URLbase + 'users/:id',
    function(req, res){
      var clienteMlab = requestJSON.createClient(baseMLabURL);
      console.log('body del get mlab: '+req.body);
      console.log('Put user Mlab');
      let id=req.params.id;
      var queryString = 'q={"id":' + id + '}&';
      var queryStrField = 'f={"_id":0}&';
      var response = {};
      clienteMlab.get ('user?' + queryString + apikeyMLab,
        function(error, respuestaMLab, body){

          if(error) {
                console.log('Error en el server');
                response = {
                "msg" : "Error en el server."
                };
                res.status(500);
             }
          else
            {
              console.log("updateID:" + id);
              var upUser = {
                 "id" : id,
                 "first_name" : req.body.first_name,
                 "last_name" : req.body.last_name,
                 "email" : req.body.email,
                 "password" : req.body.password
                   };
              console.log('antes del put'+id);
              clienteMlab.put(baseMLabURL + "user?" + queryString +apikeyMLab, upUser,
                function(error, respuestaMLab, body){
               res.send(body);
            });
        }
    });
 });


 //  Petición DELETE con id en mLab
 app.delete(URLbase + 'users/:id',
   function(req, res){
     var clienteMlab = requestJSON.createClient(baseMLabURL);
     console.log('body del get mlab: '+req.body);
     console.log('Delete user Mlab');
     let id=req.params.id;
     var queryString = 'q={"id":' + id + '}&';
     var queryStrField = 'f={"_id":0}&';
     var response = {};
     clienteMlab.get ('user?' + queryString + apikeyMLab,
       function(error, respuestaMLab, body){

         if(error) {
               console.log('Error en el server');
               response = {
               "msg" : "Error en el server."
               };
               res.status(500);
            }
         else
           {
             console.log("delete:" + id);
             clienteMlab.delete(baseMLabURL + "user?" + queryString +apikeyMLab, upUser,
               function(error, respuestaMLab, body){
              res.send(body);
           });
       }
   });
});



// ============================================

/*
// Put users
    app.put(URI+'users/:id',
    function (req, res)   {
        console.log('Put users');
        var existe = true;
        let idUpdate=req.params.id;
        if (usersFile[idUpdate-1] == undefined)
           {console.log('Usuario no existe');
            existe = false;}
       else {
           var userAct=
                 {'id':idUpdate,
                  'first_name': req.body.first_name,
                  'last_name': req.body.last_name,
                  'email': req.body.email,
                   'password': req.body.password };
                   usersFile[idUpdate-1]=userAct;
                 }
           var msgResponse = existe ? {'msg':'Update OK'} : {'msg':'Update Fallo'};
           var statusCode = existe ? 200 : 400;
           res.status(statusCode).send(msgResponse);
          });




// =====================================
//     Delete users
                app.delete(URI+'users/:id',
                        function (req, res)   {
                          console.log('Delete users');
                          var existe = true;
                          let idDelete=req.params.id;
                          if (usersFile[idDelete-1] == undefined)
                             {console.log('Usuario no existe');
                             existe = false;}
                             else {
                               usersFile.splice([idDelete-1],1);
                             }
                      app.listen(port);
console.log('Escuchando en el puerto 3000...');       var msgResponse = existe ? {'msg':'Update OK'} : {'msg':'Update Fallo'};
                             var statusCode = existe ? 200 : 400;
                             res.status(statusCode).send(msgResponse);
                      //     res.send(msgResponse) .status(statusCode) ;
                          });
*/

// Listen recibe como parametro el puerto desde el cual escucha
app.listen(port);
console.log('Escuchando en el puerto 3000...');
