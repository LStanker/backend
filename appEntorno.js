// -------------------------------------------------
// Proyecto  Techu - Ejercicio de mlab
// Version 1.0.0
// Lidice Stankervicaite -  2018
// -------------------------------------------------

// Descargo la dependencia vinculada a express
var express = require('express');
var bodyParser = require('body-parser');
var requestJSON=require('request-json');
var newID=0;
var app = express();

// entorno ----------------------------------
let entorno = require('./entorno.js');

//let app = require('./user.js');

var URLbase= entorno.URLbase ;
var baseMLabURL = entorno.baseMLabURL;
var apikeyMLab = entorno.apikeyMLab;

var port=process.env.PORT||3000;
app.use(bodyParser.json());

//=================================================================
//Metodo POST login -------------------------
app.post(URLbase + "login",
  function (req, res){
    console.log("POST login");
    var email = req.body.email;
    var pass = req.body.password;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var queryStringpass = 'q={"password":' + pass + '}&';
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('usuarios?'+ queryStringEmail + apikeyMLab ,
    function(error, respuestaMLab , body) {
      console.log("entro al body login:" + body );
      var respuesta = body[0];
      console.log(respuesta);
      // Se validan los datos recibidos -----
      if(respuesta != undefined){
          if (respuesta.password == pass) {
            console.log("Login Correcto");
            var session = {"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            console.log(baseMLabURL+'?q={"id": ' + respuesta.id + '}&' + apikeyMLab);
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(login),
              function(errorP, respuestaMLabP, bodyP) {
                res.send(body[0]);
              });
          }
          else {
            console.log("Contraseña Incorrecta");
           res.status(401).send('Autenticacion de usuario no valida');
          }
      } else {
        console.log("Email Incorrecto");
        res.status(401).send('Autenticacion de usuario no valida');
      }
    });
});


//Metodo POST logout  -------------------------
app.post(URLbase + "logout",
  function (req, res){
    console.log("POST logout");
    var email = req.body.email;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('usuarios?'+ queryStringEmail + apikeyMLab ,
    function(error, respuestaMLab, body) {
      console.log("entro al get");
      var respuesta = body[0];
      console.log(respuesta);
      // Se validan los datos recibidos -----
      if(respuesta != undefined){
            console.log("logout Correcto");
            var session = {"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            console.log(logout);
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(logout),
              function(errorP, respuestaMLabP, bodyP) {
                res.status(200).send('Logout exitoso');
              });
      } else {
        console.log("Error en Logout");
        res.status(404).send('Error en Logout');
      }
    });
});

// >>>>>>>>>> USUARIOS <<<<<<<<<<<<<<<<<<<<<<<<
// GET usuarios -------------------------
app.get(URLbase + 'usuarios',
  function(req, res) {
    console.log("GET /api-uruguay/v1");
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Usuarios HTTP mLab devueltos");
    var queryString = 'f={"_id":0}&';
    httpClient.get('usuarios?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            res.status(500).send('Error obteniendo usuarios');
        } else {
          if(body.length > 0) {
            response = body;
            res.send(response);
          } else {
              res.status(404).send('Usuarios no encontrados');
          };
        };
      });
});

// GET usuarios con id en mLab ---------------------
app.get(URLbase + 'usuarios/:id',
  function (req, res) {
    console.log("GET /api-uruguay/v1:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"idCliente":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('usuarios?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        var response = {};
        if(err) {
            res.status(500).send('Error obteniendo usuario');
          } else {
          if(body.length > 0) {
            console.log("Respuesta mLab correcta");
            response = body[0];
            res.send(response);
          } else {
            res.status(404).send('Usuario no encontrado');
          };
        };
      });
});

// POST usuarios agrega al final ---------------------
app.post(URLbase + 'usuarios',
  function(req, res){
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Entro al post.");
    httpClient.get('usuarios?' + apikeyMLab,
      function(error, respuestaMLab, body){
        newID = body.length + 1;
        console.log(req.body);
        console.log("newID:" + newID);
        var newUser = {
          "idCliente" : newID,
          "nombre" : req.body.nombre,
          "apellido" : req.body.apellido,
          "email" : req.body.email,
          "password" : req.body.password
           };
            httpClient.post(baseMLabURL + "usuarios?" + apikeyMLab, newUser,
            function(error, respuestaMLab, body){
            if (error) {
                  res.status(500).send('Error al grabar');
              }
              else {
                 res.send(body); // Muestra los datos
                 };
              });
      });
  });

  //PUT usuarios con id en mLab ---------------------
  app.put(URLbase + 'usuarios/:id',
    function (req, res) {
      console.log("put /api-uruguay/v1:id");
      console.log(req.params.id);
      var id = req.params.id;
      var queryString = 'q={"idCliente":' + id + '}&';
      var httpClient = requestJSON.createClient(baseMLabURL);
      httpClient.get('usuarios?' + queryString  + apikeyMLab,
        function(err, respuestaMLab, body){
          console.log("Respuesta mLab correcta.");
          if(err) {
             res.status(500).send('Error obteniendo usuario');
          } else {
            if(body.length > 0) {
                    var userIdInterno =  body[0]._id.$oid; // id de mongo
                    var clienteMlab = requestJSON.createClient(baseMLabURL);
                     console.log("Respuesta mLab id interno."+  body[0]._id.$oid);
                    clienteMlab.put(baseMLabURL + "usuarios/"+ userIdInterno + '?'+ apikeyMLab, req.body,
                       function(err, respuestaMLab, usrResponse){
                      res.send(usrResponse);
                     });
            }
            else {
                 res.status(404).send('Usuario no encontrado');
            };
          };
        });
    });

    // Delete usuarios con id en mLab ---------------------
    app.delete(URLbase + 'usuarios/:id',
      function (req, res) {
        console.log("delete /api-uruguay/v1:id");
        console.log(req.params.id);
        var id = req.params.id;
        var queryString = 'q={"idCliente":' + id + '}&';
        var httpClient = requestJSON.createClient(baseMLabURL);
        httpClient.get('usuarios?' + queryString  + apikeyMLab,
          function(err, respuestaMLab, body){
            console.log("Respuesta mLab correcta.");
            var response = {};
            if(err) {
                 res.status(500).send('Error obteniendo usuario');
            } else {
              if(body.length > 0) {
                  // Obtiene el primer elemento del array
                  response = body[0];
                  var userIdInterno =  body[0]._id.$oid; // id de mongo
                  var clienteMlab = requestJSON.createClient(baseMLabURL);
                   console.log("Respuesta mLab id interno."+  body[0]._id.$oid);
                   clienteMlab.delete(baseMLabURL + "usuarios/"+ userIdInterno + '?'+ apikeyMLab,
                     function(err, respuestaMLab, usrResponse){
                      res.status(200).send('Usuario Borrado');
                   });
              } else {
                   res.status(404).send('Usuario no encontrado');
              }
            }
          });
      });


      // >>>>>>>>>> CUENTAS <<<<<<<<<<<<<<<<<<<<<<<<
      // GET cuentas -------------------------
      app.get(URLbase + 'cuentas',
        function(req, res) {
          console.log("GET /api-uruguay/v1");
          var httpClient = requestJSON.createClient(baseMLabURL);
          console.log("Cuenta HTTP mLab devueltas");
          var queryString = 'f={"_id":0}&';
          httpClient.get('cuentas?' + queryString + apikeyMLab,
            function(err, respuestaMLab, body) {
              var response = {};
              if(err) {
                  res.status(500).send('Error obteniendo cuentas');
              } else {
                if(body.length > 0) {
                  response = body;
                  res.send(response);
                } else {
                    res.status(404).send('Cuentas no encontrado');
                };
              };
            });
      });

      // GET cuentas con id en mLab ---------------------
      app.get(URLbase + 'cuentas/:id',
        function (req, res) {
          console.log("GET /api-uruguay/v1:id");
          console.log(req.params.id);
          var id = req.params.id;
          var queryString = 'q={"idCuenta":' + id + '}&';
          var queryStrField = 'f={"_id":0}&';
          var httpClient = requestJSON.createClient(baseMLabURL);
          httpClient.get('cuentas?' + queryString + queryStrField + apikeyMLab,
            function(err, respuestaMLab, body){
              var response = {};
              if(err) {
                  res.status(500).send('Error obteniendo Cuenta');
                } else {
                if(body.length > 0) {
                  console.log("Respuesta mLab correcta");
                  response = body[0];
                  res.send(response);
                } else {
                  res.status(404).send('Cuenta no encontrada');
                };
              };
            });
      });

      // POST cuentas agrega al final ---------------------
      app.post(URLbase + 'cuentas',
        function(req, res) {
          var httpClient = requestJSON.createClient(baseMLabURL);
          console.log("Entro al post cuentas");
          httpClient.get('cuentas?' + apikeyMLab,
            function(err, respuestaMLab, body){
              if(err) {
                  res.status(500).send('Error obteniendo cuentas');
              } else {
                    newID = body.length + 1;
                    console.log(req.body);
                    console.log("newID:" + newID);
                    var newCuenta = {
                      "idCuenta" : newID,
                      "cliente" : req.body.cliente,
                      "iban" : req.body.iban,
                      "movs" : req.body.movs
                       };
                       console.log("Detalle cuenta"+ newCuenta );

                        httpClient.post(baseMLabURL + "cuentas?" + apikeyMLab, newCuenta,
                        function(error, respuestaMLab, body){
                        if (error) {
                              res.status(500).send('Error al grabar');
                          }
                          else {
                             res.send(body); // Muestra los datos
                           }; // if error
                         }); //   httpClient post
               } // if err
            }); //   httpClient get
        });



// Listen recibe como parametro el puerto desde el cual escucha
app.listen(port);
console.log('Escuchando en el puerto 3000...');
module.exports.app = app;
