BACK:
Se pueden utilizar las siguientes rutinas que están vinculadas a:

Usuarios: Esta tabla tiene todos los clientes usuarios del sistema.
Por cada usuario se guardan:
   idCliente (numérico): Autonumérico generado por el sistema.
   nombre (string): Nombre del cliente.
   apellido (string): Apellido del cliente.
   email (string): email que identifica al cliente en el sistema y con el cual se loguea.
   password (string): contraseña de acceso al sistema.
Y se agrega la marca de logueado en función de que esté conectado o no al sistema.

login: Permite acceso al sistema controlando mail y contraseña, en caso de autenticación correcta agrega el campo logueado  al registro encontrado (los datos se reciben en el body).   
Método: PUT
Parámetros de entrada: email, password
Versión: 1.0.0

logout: Permite salir del sistema accediendo por email,  en caso de que se encuentre al usuario se elimina el campo logueado del registro encontrado.      
Método: PUT
Parámetros de entrada: email
Versión: 1.0.0

usuarios: Devuelve todos los usuarios del sistema.
Método: GET
Parámetros de entrada: N/A
Versión: 1.0.0

usuarios/:id: Devuelve los datos del usuario indicado.
Método: GET
Parámetros de entrada: idCliente
Versión: 1.0.0

usuarios: Agrega los datos del usuario que recibe en el body al final de la tabla.
Método: POST
Parámetros de entrada: Datos del usuario se reciben el el body.
Versión: 1.0.0
usuarios/:id: Permite modificar los datos del usuario indicado.
Método:PUT
Parámetros de entrada: idCliente
Versión: 1.0.0

usuarios/:id: Permite borrar el usuario indicado.
Método:DELETE
Parámetros de entrada: idCliente
Versión: 1.0.0

mail/:id: Devuelve los datos de un usuario accediendo por email.
Método:GET
Parámetros de entrada: email
Versión: 1.0.0

Cuentas: Esta tabla contiene el detalle de todas las cuentas vinculadas a clientes y por cada una de ellas un array con los movimientos realizados.
Por cada cuenta se guardan:
idCuenta (numérico) : Autonumérico generado por el sistema.
cliente (numérico): Id de cliente y que lo vincula con la tabla de usuarios.
iban (string): IBAN de la cuenta.
movs (array), cada elemento del array tiene:
    id (numérico): autonumérico de movimiento.
    fecha  (fecha): Fecha en que se realizó el movimiento.
    imp (numérico): Importe del movimiento.
    tipo (string): Indica si se trata de un movimiento de debito o credito.
    moneda (string): Codigo de moneda del movimiento realizado.


cuentas: Devuelve todos las cuentas del sistema.
Método: GET
Parámetros de entrada: N/A
Versión: 1.0.0

cuentaId/:id/:cli: Devuelve los datos de una cuenta de un cliente en particular.
Método: GET
Parámetros de entrada: idCuenta, idCliente
Versión: 1.0.0

cuentas/:id: Devuelve todas las cuentas y movimientos de un cliente en particular.
Método: GET
Parámetros de entrada: idCliente
Versión: 1.0.0

ctas/:id: Devuelve las cuentas de un cliente en particular.
Método: GET
Parámetros de entrada: idCliente
Versión: 1.0.0

clienteMov/:id: Devuelve los movimientos de una cuenta accediendo por id de cuenta.
Método: GET
Parámetros de entrada: idCliente
Versión: 1.0.0

cuentas: Agrega los datos de cuentas y movimientos que recibe en el body al final de la tabla.
Método: POST
Parámetros de entrada: Datos de la cuenta se reciben el el body.
Versión: 1.0.0

cuentas/:id: Permite modificar los datos de la cuenta indicada.
Método:PUT
Parámetros de entrada: idCuenta
Versión: 1.0.0

cuentas/:id: Permite borrar los datos de la cuenta indicada.
Método:DELETE
Parámetros de entrada: idCuenta
Versión: 1.0.0
