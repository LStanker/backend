# Imagen base de node la mas reciente
FROM node:latest

# Crea el directorio de Docker
WORKDIR /docker-api

# Copia archivos del proyecto a /api-uruguay
ADD . /docker-api

# Le digo que intale npm x que no lo salve al tener ignore en el package.json
# Importante para instalar las dependencias del Proyecto
# RUN npm install

# Puerto donde exponemos el contenedor
EXPOSE 3000

# Comando para lanzar la app
CMD ["npm", "start"]
