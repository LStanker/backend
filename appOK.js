// -------------------------------------------------
// Proyecto  Techu - Ejercicio de mlab
// Version 1.0.0
// Lidice Stankervicaite -  2018
// -------------------------------------------------

// Descargo la dependencia vinculada a express
var express = require('express');
var bodyParser = require('body-parser');
var requestJSON=require('request-json');
var newID=0;
var app = express();

// Entorno ----------------------------------
let entorno = require('./entorno.js');

var URLbase= entorno.URLbase ;
var baseMLabURL = entorno.baseMLabURL;
var apikeyMLab = entorno.apikeyMLab;
var port= entorno.port;
// =====---------------------------------

app.use(bodyParser.json());

//========================= RUTINAS =======================================

//=====================================================================
// >>>>>>>>>>>>>>>>>>>>     USUARIOS    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//=====================================================================

//-----------------------------------------------------------------------
// --- Permite acceso al sistema controlando mail y contraseña      ---
// --- en caso de autenticacion correcta agrega el campo logueado   ---
// --- al registro encontrado (los datos se reciben en el body).    ---
//Metodo PUT login / usuarios(email, password) - V 1.0.0 -----------------
app.put(URLbase + "login",
  function (req, res){
  //  console.log("Login");
    var email = req.body.email;
    var pass = req.body.password;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var queryStringpass = 'q={"password":' + pass + '}&';
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('usuarios?'+ queryStringEmail + apikeyMLab ,
    function(error, respuestaMLab , body) {
      var respuesta = body[0];
      // Se validan los datos recibidos -----
      if(respuesta != undefined){
          if (respuesta.password == pass) {
  //          console.log("Login Correcto");
            var session = {"logueado":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            var userIdInterno =  body[0]._id.$oid; // id de mongo
            clienteMlab.put(baseMLabURL + "usuarios/"+ userIdInterno + '?'+ apikeyMLab, JSON.parse(login),
              function(errorP, respuestaMLabP, bodyP) {
                res.send(bodyP);
              });
          }
          else {
  //         console.log("Contraseña Incorrecta");
           res.status(401).send('Autenticacion de usuario no valida');
          }
      } else {
  //      console.log("Email Incorrecto");
        res.status(401).send('Autenticacion de usuario no valida');
      }
    });
});

//-----------------------------------------------------------------------
// --- Permite salir del sistema accediendo por email       -----------
// --- en caso de que se encuentre al usuario se elimina el         ---
// --- campo logueado del registro encontrado.                      ---
//Metodo PUT logout / usuarios(email) - V 1.0.0 -------------------------
app.put(URLbase + "logout",
  function (req, res){
//    console.log("Logout");
    var email = req.body.email;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('usuarios?'+ queryStringEmail + apikeyMLab ,
    function(error, respuestaMLab, body) {
      var respuesta = body[0];
      // Se validan los datos recibidos -----
      if(respuesta != undefined){
  //          console.log("Logout Correcto");
            var session = {"logueado":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            var userIdInterno =  body[0]._id.$oid; // id de mongo
            clienteMlab.put(baseMLabURL + "usuarios/"+ userIdInterno + '?'+ apikeyMLab, JSON.parse(logout),
            function(errorP, respuestaMLabP, bodyP) {
                    res.send(bodyP);
              });
      } else {
//        console.log("Error en Logout");
        res.status(404).send('Error en Logout');
      }
    });
});


//-----------------------------------------------------------------------
// --- Devuelve todos los usuarios del sistema --------------------------
//Metodo GET usuarios  - V 1.0.0 ----------------------------------------
app.get(URLbase + 'usuarios',
  function(req, res) {
//    console.log("GET /api-uruguay/v1");
    var httpClient = requestJSON.createClient(baseMLabURL);
//    console.log("Usuarios HTTP mLab devueltos");
    var queryString = 'f={"_id":0}&';
    httpClient.get('usuarios?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            res.status(500).send('Error obteniendo usuarios');
        } else {
          if(body.length > 0) {
            response = body;
            res.send(response);
          } else {
              res.status(404).send('Usuarios no encontrados');
          };
        };
      });
});

//---------------------------------------------------------------------
// --- Devuelve los datos de un usuario       -------------------------
// --- accediendo por id de cliente         ---------------------------
//Metodo GET / usuarios(idCliente) - V 1.0.0 --------------------------
app.get(URLbase + 'usuarios/:id',
  function (req, res) {
//    console.log("GET /api-uruguay/v1:id");
//    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"idCliente":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('usuarios?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        var response = {};
        if(err) {
            res.status(500).send('Error obteniendo usuario');
          } else {
          if(body.length > 0) {
//            console.log("Respuesta mLab correcta");
            response = body[0];
            res.send(response);
          } else {
            res.status(404).send('Usuario no encontrado');
          };
        };
      });
});

//-----------------------------------------------------------------------
// --- Agrega un usuario al final      ---------------------------------
// --- genera automaticamente el id de cliente           ---------------
// --- los datos se reciben en el body                      ------------
//Metodo POST / usuarios - V 1.0.0 --------------------------------------
app.post(URLbase + 'usuarios',
  function(req, res){
    var httpClient = requestJSON.createClient(baseMLabURL);
//    console.log("Entro al post.");
    httpClient.get('usuarios?' + apikeyMLab,
      function(error, respuestaMLab, body){
        newID = body.length + 1;
//        console.log(req.body);
//        console.log("newID:" + newID);
        var newUser = {
          "idCliente" : newID,
          "nombre" : req.body.nombre,
          "apellido" : req.body.apellido,
          "email" : req.body.email,
          "password" : req.body.password
           };
            httpClient.post(baseMLabURL + "usuarios?" + apikeyMLab, newUser,
            function(error, respuestaMLab, body){
            if (error) {
                  res.status(500).send('Error al grabar');
              }
              else {
                 res.send(body); // Muestra los datos
                 };
              });
      });
  });

//-----------------------------------------------------------------------
// --- Modifica los datos del usuario que recibe como ------------------
// --- parametro, los datos para la actualizacion        ---------------
// --- se reciben en el body                      ----------------------
//Metodo PUT / usuarios(idCliente) - V 1.0.0 ----------------------------
  app.put(URLbase + 'usuarios/:id',
    function (req, res) {
//      console.log("put /api-uruguay/v1:id");
//      console.log(req.params.id);
      var id = req.params.id;
      var queryString = 'q={"idCliente":' + id + '}&';
      var httpClient = requestJSON.createClient(baseMLabURL);
      httpClient.get('usuarios?' + queryString  + apikeyMLab,
        function(err, respuestaMLab, body){
//          console.log("Respuesta mLab correcta.");
          if(err) {
             res.status(500).send('Error obteniendo usuario');
          } else {
            if(body.length > 0) {
                    var userIdInterno =  body[0]._id.$oid; // id de mongo
                    var clienteMlab = requestJSON.createClient(baseMLabURL);
//                     console.log("Respuesta mLab id interno."+  body[0]._id.$oid);
                    clienteMlab.put(baseMLabURL + "usuarios/"+ userIdInterno + '?'+ apikeyMLab, req.body,
                       function(err, respuestaMLab, usrResponse){
                      res.send(usrResponse);
                     });
            }
            else {
                 res.status(404).send('Usuario no encontrado');
            };
          };
        });
    });

//-----------------------------------------------------------------------
// --- Borra los datos del usuario que recibe como --------------------
// --- parametro.                                       ---------------
//Metodo DELETE / usuarios(idCliente) - V 1.0.0 --------------------------
  app.delete(URLbase + 'usuarios/:id',
    function (req, res) {
  //      console.log("delete /api-uruguay/v1:id");
  //     console.log("delete "+ req.params.id);
      var id = req.params.id;
      var queryString = 'q={"idCliente":' + id + '}&';
      var httpClient = requestJSON.createClient(baseMLabURL);
      httpClient.get('usuarios?' + queryString  + apikeyMLab,
        function(err, respuestaMLab, body){
  //          console.log("Respuesta mLab correcta.");
          var response = {};
          if(err) {
               res.status(500).send('Error obteniendo usuario');
          } else {
            if(body.length > 0) {
                // Obtiene el primer elemento del array
                response = body[0];
                var userIdInterno =  body[0]._id.$oid; // id de mongo
                var clienteMlab = requestJSON.createClient(baseMLabURL);
  //                 console.log("Respuesta mLab id interno."+  body[0]._id.$oid);
                 clienteMlab.delete(baseMLabURL + "usuarios/"+ userIdInterno + '?'+ apikeyMLab,
                   function(err, respuestaMLab, usrResponse){
                    res.status(200).send('Usuario Borrado');
                 });
            } else {
                 res.status(404).send('Usuario no encontrado');
            }
          }
        });
    });

//---------------------------------------------------------------------
// --- Devuelve datos de un usuario  ----------------------------------
// --- accediendo por email      --------------------------------------
//Metodo GET / mail(email) - V 1.0.0 ----------------------------------
//"q" example - return all documents with "active" field of true:
// https://api.mlab.com/api/1/databases/my-db/collections/my-coll?q={"active": true}&apiKey=myAPIKey
app.get(URLbase + 'mail/:id',
  function (req, res) {
//    console.log("GET mail");
//    console.log("req.params.id "+ req.params.id);
    var id = req.params.id;
    var queryString = 'q={"email":' +'"' +id + '"' +'}&';
//      console.log("queryString " + queryString);
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('usuarios?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body){
        var response = {};
        if(err) {
            res.status(500).send('Error obteniendo usuario');
          } else {
          if(body.length > 0) {
//            console.log("Respuesta mLab correcta");
            response = body[0];
            res.send(response);
          } else {
            res.status(404).send('Usuario no encontrado');
          };
        };
      });
});



//=====================================================================
// >>>>>>>>>>>>>>>>>>>>     CUENTAS    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//=====================================================================

//-----------------------------------------------------------------------
// --- Devuelve todas las cuentas del sistema --------------------------
//Metodo GET cuentas  - V 1.0.0 ----------------------------------------
    app.get(URLbase + 'cuentas',
    function(req, res) {
//        console.log("GET /api-uruguay/v1");
      var httpClient = requestJSON.createClient(baseMLabURL);
//        console.log("Cuenta HTTP mLab devueltas");
      var queryString = 'f={"_id":0}&';
      httpClient.get('cuentas?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body) {
          var response = {};
          if(err) {
              res.status(500).send('Error obteniendo cuentas');
          } else {
            if(body.length > 0) {
              response = body;
              res.send(response);
            } else {
                res.status(404).send('Cuentas no encontrado');
            };
          };
        });
  });

  //---------------------------------------------------------------------
  // --- Devuelve los datos de una cuenta       ------------------------
  // --- accediendo por id de cuenta         ---------------------------
  //Metodo GET / cuentas(idCuenta) - V 1.0.0 ----------------------------
    app.get(URLbase + 'cuentaId/:id/:cli',
      function (req, res) {
        var id = req.params.id;
        var cli = req.params.cli;
        var queryString = 'q={"idCuenta":' + id + ', "cliente":' + cli +'}&';
    //    console.log("queryString " + queryString);
        var queryStrField = 'f={"_id":0}&';
        var httpClient = requestJSON.createClient(baseMLabURL);
        httpClient.get('cuentas?' + queryString + queryStrField + apikeyMLab,
          function(err, respuestaMLab, body){
            var response = {};
            if(err) {
                res.status(500).send('Error obteniendo Cuenta');
              } else {
              if(body.length > 0) {
        //        console.log("Respuesta mLab correcta");
                response = body[0];
                res.send(response);
              } else {
                res.status(404).send('Cuenta no encontrada');
              };
            };
          });
    });
//---------------------------------------------------------------------
// --- Devuelve  las cuentas de un cliente       ----------------------
// --- accediendo por id de cliente              ----------------------
//Metodo GET / cuentas(idCuenta) - V 1.0.0 ----------------------------
  app.get(URLbase + 'cuentas/:id',
    function (req, res) {
  //    console.log("GET /api-uruguay/v1:id");
  //    console.log(req.params.id);
      var id = req.params.id;
      var queryString = 'q={"cliente":' + id + '}&';
      var queryStrField = 'f={"_id":0}&';
      var httpClient = requestJSON.createClient(baseMLabURL);
      httpClient.get('cuentas?' + queryString + queryStrField + apikeyMLab,
        function(err, respuestaMLab, body){
          var response = {};
          if(err) {
              res.status(500).send('Error obteniendo Cuentas del cliente');
            } else {
            if(body.length > 0) {
      //        console.log("Respuesta mLab correcta");
              response = body[0];
              res.send(response);
            } else {
              res.status(404).send('Cuentas del cliente no encontradas');
            };
          };
        });
  });

  //---------------------------------------------------------------------
  // --- Devuelve  las cuentas de un cliente       ----------------------
  // --- accediendo por id de cliente sin sus movientos------------------
  //Metodo GET / cuentas(idCuenta) - V 1.0.0 ----------------------------
    app.get(URLbase + 'ctas/:id',
      function (req, res) {
    //    console.log("GET /api-uruguay/v1:id");
    //    console.log(req.params.id);
        var id = req.params.id;
        var queryString = 'q={"cliente":' + id + '}&';
        var queryStrField = 'f={"_id":0, "idCuenta":1, "cliente":1, "iban":1}&';
        var httpClient = requestJSON.createClient(baseMLabURL);
        httpClient.get('cuentas?' + queryString + queryStrField + apikeyMLab,
              function(err, respuestaMLab, body){
            var response = {};
            if(err) {
                res.status(500).send('Error obteniendo Cuentas del cliente');
              } else {
              if(body.length > 0) {
        //        console.log("Respuesta mLab correcta");
                response = body;
                res.send(response);
              } else {
                res.status(404).send('Cuentas del cliente no encontradas');
              };
            };
          });
    });

//---------------------------------------------------------------------
// --- Devuelve los movientos de una cuenta       ------------------------
// --- accediendo por id de cuenta         ---------------------------
//Metodo GET / clienteMov(idCuenta) - V 1.0.0 ----------------------------
  app.get(URLbase + 'clienteMov/:id',
    function (req, res) {
//          console.log("GET /api-uruguay/v1:id");
//          console.log(req.params.id);
      var id = req.params.id;
      var queryString = 'q={"cliente":' + id + '}&';
       var queryStrField = 'f={"_id":0}&';
//      var queryStrField = 'f={"_id":0, "idCuenta":0,"iban":0}&';
      var httpClient = requestJSON.createClient(baseMLabURL);
      httpClient.get('cuentas?' + queryString + queryStrField + apikeyMLab,
        function(err, respuestaMLab, body){
          var response = {};
          if(err) {
              res.status(500).send('Error obteniendo Cuenta');
            } else {
            if(body.length > 0) {
      //        console.log("Respuesta mLab correcta");
              response = body[0];
              res.send(response);
            } else {
              res.status(404).send('Cuenta no encontrada');
            };
          };
        });
  });

//-----------------------------------------------------------------------
// --- Agrega una cuenta al final      ---------------------------------
// --- genera automaticamente el id de cuenta           ----------------
// --- los datos se reciben en el body                      ------------
//Metodo POST / cuentas - V 1.0.0 --------------------------------------
  app.post(URLbase + 'cuentas',
    function(req, res) {
      var httpClient = requestJSON.createClient(baseMLabURL);
//        console.log("Entro al post cuentas");
      httpClient.get('cuentas?' + apikeyMLab,
        function(err, respuestaMLab, body){
          if(err) {
              res.status(500).send('Error obteniendo cuentas');
          } else {
                newID = body.length + 1;
//                  console.log(req.body);
//                  console.log("newID:" + newID);
                var newCuenta = {
                  "idCuenta" : newID,
                  "cliente" : req.body.cliente,
                  "iban" : req.body.iban,
                  "movs" : req.body.movs
                   };
//                       console.log("Detalle cuenta"+ newCuenta );
                    httpClient.post(baseMLabURL + "cuentas?" + apikeyMLab, newCuenta,
                    function(error, respuestaMLab, body){
                    if (error) {
                          res.status(500).send('Error al grabar');
                      }
                      else {
                         res.send(body); // Muestra los datos
                       }; // if error
                     }); //   httpClient post
           } // if err
        }); //   httpClient get
    });

//---------------------------------------------------------------------
// --- Modifica los datos de la cuenta que recibe como ----------------
// --- parametro, los datos para la actualizacion        --------------
// --- se reciben en el body                      ---------------------
//Metodo PUT / cuentas(idCuenta) - V 1.0.0 ----------------------------
  app.put(URLbase + 'Cuentas/:id',
    function (req, res) {
//            console.log("put /api-uruguay/v1:id");
//            console.log(req.params.id);
      var id = req.params.id;
      var queryString = 'q={"idCuenta":' + id + '}&';
      var httpClient = requestJSON.createClient(baseMLabURL);
      httpClient.get('cuentas?' + queryString  + apikeyMLab,
        function(err, respuestaMLab, body){
//                console.log("Respuesta mLab correcta.");
          if(err) {
             res.status(500).send('Error obteniendo cuenta');
          } else {
            if(body.length > 0) {
                    var userIdInterno =  body[0]._id.$oid; // id de mongo
                    var clienteMlab = requestJSON.createClient(baseMLabURL);
//                         console.log("Respuesta mLab id interno."+  body[0]._id.$oid);
                    clienteMlab.put(baseMLabURL + "cuentas/"+ userIdInterno + '?'+ apikeyMLab, req.body,
                       function(err, respuestaMLab, usrResponse){
                      res.send(usrResponse);
                     });
            }
            else {
                 res.status(404).send('Cuenta no encontrada');
            };
          };
        });
    });

//-----------------------------------------------------------------------
// --- Borra los datos de la cuenta  que recibe como --------------------
// --- parametro.                                       ---------------
//Metodo DELETE / cuentas(idCuenta) - V 1.0.0 --------------------------
  app.delete(URLbase + 'cuentas/:id',
    function (req, res) {
  //    console.log("delete /api-uruguay/v1:id");
  //    console.log(req.params.id);
      var id = req.params.id;
      var queryString = 'q={"idCuenta":' + id + '}&';
      var httpClient = requestJSON.createClient(baseMLabURL);
      httpClient.get('cuentas?' + queryString  + apikeyMLab,
        function(err, respuestaMLab, body){
  //        console.log("Respuesta mLab correcta.");
          var response = {};
          if(err) {
               res.status(500).send('Error obteniendo cuentas');
          } else {
            if(body.length > 0) {
                // Obtiene el primer elemento del array
                response = body[0];
                var userIdInterno =  body[0]._id.$oid; // id de mongo
                var clienteMlab = requestJSON.createClient(baseMLabURL);
    //             console.log("Respuesta mLab id interno."+  body[0]._id.$oid);
                 clienteMlab.delete(baseMLabURL + "cuentas/"+ userIdInterno + '?'+ apikeyMLab,
                   function(err, respuestaMLab, usrResponse){
                    res.status(200).send('Cuenta Borrada');
                 });
            } else {
                 res.status(404).send('Cuenta no encontrada');
            }
          }
        });
    });


  //=====================================================================
  // >>>>>>>>>>>>>>>>>>>>    users - visto en clase<<<<<<<<<<<<<<<<<<<<<
  //=====================================================================

// GET users consumiendo API REST de mLab
app.get(URLbase + 'users',
  function(req, res) {
//    console.log("GET /api-uruguay/v1");
    var httpClient = requestJSON.createClient(baseMLabURL);
//    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});

// Petición GET con id en mLab
app.get(URLbase + 'users/:id',
  function (req, res) {
//    console.log("GET /api-uruguay/v1:id");
//    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
//        console.log("Respuesta mLab correcta.");
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.sendStatus(500);
        } else {
          if(body.length > 0) {
            response = body[0];
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.sendStatus(404);
          }
        }
        res.send(response);
      });
});

// Petición POST con id en mLab
app.post(URLbase + 'users',
  function(req, res){
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?' + apikeyMLab,
      function(error, respuestaMLab, body){
        newID = body.length + 1;
//        console.log(req.body);
//        console.log("newID:" + newID);
        var newUser = {
          "id" : newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
           };

            clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, newUser,
            function(error, respuestaMLab, body){
              res.send(body); //aqui se puede controlar error con la variable error

          });
      });
  });


// Petición PUT con id en mLab
app.put(URLbase + 'users/:id',
  function (req, res) {
//    console.log("put /api-uruguay/v1:id");
//    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString  + apikeyMLab,
      function(err, respuestaMLab, body){
//        console.log("Respuesta mLab correcta.");
        var response = {};
        if(err) {
           res.status(500).send('Error obteniendo usuario');
        } else {
          if(body.length > 0) {
              // Obtiene el primer elemento del array
                  response = body[0];
                  var userIdInterno =  body[0]._id.$oid; // id de mongo
                  var clienteMlab = requestJSON.createClient(baseMLabURL);
  //                 console.log("Respuesta mLab id interno."+  body[0]._id.$oid);
                   clienteMlab.put("user/"+ userIdInterno + '?'+ apikeyMLab, req.body,
                     function(err, respuestaMLab, usrResponse){
                    res.send(usrResponse);
                   });
          }
          else {
               res.status(404).send('Usuario no encontrado');
          };
        };
      });
  });


    // Petición delete con id en mLab
    app.delete(URLbase + 'users/:id',
      function (req, res) {
  //      console.log("put /api-uruguay/v1:id");
  //      console.log(req.params.id);
        var id = req.params.id;
        var queryString = 'q={"id":' + id + '}&';
        var httpClient = requestJSON.createClient(baseMLabURL);
        httpClient.get('user?' + queryString  + apikeyMLab,
          function(err, respuestaMLab, body){
  //          console.log("Respuesta mLab correcta.");
            var response = {};
            if(err) {
                 res.status(500).send('Error obteniendo usuario');
            } else {
              if(body.length > 0) {
                  // Obtiene el primer elemento del array
                  response = body[0];
                  var userIdInterno =  body[0]._id.$oid; // id de mongo
                  var clienteMlab = requestJSON.createClient(baseMLabURL);
    //               console.log("Respuesta mLab id interno."+  body[0]._id.$oid);
                   clienteMlab.delete("user/"+ userIdInterno + '?'+ apikeyMLab,
                     function(err, respuestaMLab, usrResponse){
                      res.status(200).send('Usuario Borrado');
                   });
              } else {
                   res.status(404).send('Usuario no encontrado');
              }
            }
          });
      });


// Listen recibe como parametro el puerto desde el cual escucha
app.listen(port);
console.log('Escuchando en el puerto 3000...');
