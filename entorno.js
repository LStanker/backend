
// -------------------------------------------------
// Proyecto  Techu - Ejercicio de mlab
// Version 1.0.0
// Lidice Stankervicaite -  2018
// -------------------------------------------------


// Variables y constantes
const URLbase= '/api-uruguay/v1/';
var baseMLabURL = "https://api.mlab.com/api/1/databases/techubduruguay/collections/";
var apikeyMLab = "apiKey=lK0qtGN5GVG_goeTGdWeGOCWwU8TLvuV";
var port=process.env.PORT||3000;

module.exports.URLbase  = URLbase;
module.exports.baseMLabURL =  baseMLabURL;
module.exports.apikeyMLab  = apikeyMLab ;
module.exports.port  = port;
